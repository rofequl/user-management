$(document).ready(function () {
    view(5);
});

function view(value) {
    $("#view").removeClass('Dashboard UserProfile AllUser Security');
    $(".CG-1, .CG-2, .CG-3, .CG-4").css({'color': '#b38c8c'});
    switch (value) {
        case 5:
            $("#view").addClass('Dashboard');
            $(".CG-1").css({'color': 'White'});
            Dashboard();
            break;
        case 4:
            $("#view").addClass('UserProfile');
            $(".CG-2").css({'color': 'White'});
            UserProfile();
            break;
        case 3:
            $("#view").addClass('AllUser');
            $(".CG-3").css({'color': 'White'});
            AllUser();
            break;
        case 2:
            $("#view").addClass('Security');
            $(".CG-4").css({'color': 'White'});
            Setting();
    }
}

function Dashboard() {
    let Obj = '["first_name","last_name","email","gender"]';
    $.post('home/crud.php?action=dashboard', {
        PostTable: "sign_up",
        PostData: Obj,
        PostUsername: CookeValue('username=')
    }, function (data) {
        JSON.parse(data).forEach(function (element) {
            let result = '<div class="widget-box rounded">\n' +
                '                <table class="table rounded">\n' +
                '                    <thead class="thead-dark">\n' +
                '                        <tr>\n' +
                '                            <th colspan="3">\n' +
                '                                Personal Info\n' +
                '                            </th>\n' +
                '                        </tr>\n' +
                '                    </thead>\n' +
                '                    <tr>\n' +
                '                        <td>Name:</td>\n' +
                '                        <td>' + element.first_name + ' ' + element.last_name + '</td>\n' +
                '                    </tr>\n' +
                '                    <tr>\n' +
                '                        <td>Email:</td>\n' +
                '                        <td>' + element.email + '</td>\n' +
                '                    </tr>\n' +
                '                    <tr>\n' +
                '                        <td>Gender:</td>\n' +
                '                        <td>' + element.gender + '</td>\n' +
                '                    </tr>\n' +
                '                </table>\n' +
                '            </div>';
            $(".Dashboard").html(result);
        });
    });
}

function UserProfile() {
    document.cookie = "Change=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    let Obj = '["first_name","last_name","email","gender","b_day","b_month","b_year"]';
    let Obj2 = '["*"]';
    $.post('home/crud.php?action=dashboard', {
            PostTable: "sign_up",
            PostData: Obj,
            PostUsername: CookeValue('username=')
        },
        function (data) {
            JSON.parse(data).forEach(function (element) {
                $.post('home/crud.php?action=dashboard', {
                        PostTable: "about",
                        PostData: Obj2,
                        PostUsername: CookeValue('username=')
                    },
                    function (data2) {
                        JSON.parse(data2).forEach(function (element2) {
                            let result = '<div class="widget-box rounded">\n' +
                                '                <table class="table rounded">\n' +
                                '                    <thead class="thead-dark">\n' +
                                '                    <tr>\n' +
                                '                        <th colspan="4">\n' +
                                '                            User Profile\n' +
                                '                        </th>\n' +
                                '                    </tr>\n' +
                                '                    </thead>\n' +
                                '                    <tbody class="td-size">\n' +
                                '                    <tr>\n' +
                                '                        <td>Name:</td>\n' +
                                '                        <td class="input1">' + element.first_name + ' ' + element.last_name + '</td>\n' +
                                '                        <td>Email</td>\n' +
                                '                        <td class="input2">' + element.email + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Relationship:</td>\n' +
                                '                        <td class="input3">' + element2.relationship + '</td>\n' +
                                '                        <td>Phone:</td>\n' +
                                '                        <td class="input4">' + element2.phone + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Gender:</td>\n' +
                                '                        <td class="input5">' + element.gender + '</td>\n' +
                                '                        <td>Birthday:</td>\n' +
                                '                        <td class="input6">' + element.b_day + '-' + element.b_month + '-' + element.b_year + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Language:</td>\n' +
                                '                        <td class="input7">' + element2.languages + '</td>\n' +
                                '                        <td>Religious:</td>\n' +
                                '                        <td class="input8">' + element2.religious_views + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>High School:</td>\n' +
                                '                        <td class="input9">' + element2.high_school + '</td>\n' +
                                '                        <td>College:</td>\n' +
                                '                        <td class="input10">' + element2.collage + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Current City:</td>\n' +
                                '                        <td class="input11">' + element2.current_city + '</td>\n' +
                                '                        <td>Hometown:</td>\n' +
                                '                        <td class="input12">' + element2.hometown + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Interest in:</td>\n' +
                                '                        <td class="input13">' + element2.interested_in + '</td>\n' +
                                '                        <td>Political:</td>\n' +
                                '                        <td class="input14">' + element2.political_views + '</td>\n' +
                                '                    </tr>\n' +
                                '                    <tr>\n' +
                                '                        <td>Details about you:</td>\n' +
                                '                        <td colspan="3" class="input15">' + element2.details_about_you + '</td>\n' +
                                '                    </tr>\n' +
                                '\n' +
                                '                    </tbody>\n' +
                                '                </table>\n' +
                                '                <div class="updateInfoBtn"><button class="float-right btn btn-secondary btn-sm" onclick="UpdateInfo()">Update Info</button></div>\n' +
                                '            </div>';
                            $(".UserProfile").html(result);
                        });
                    });
            });
        });
}

function AllUser() {
    let Obj = '["first_name","last_name","email","pro_pic","username"]';
    $.post('home/crud.php?action=dashboard', {
        PostTable: "sign_up",
        PostData: Obj
    }, function (data) {
        let myObj = JSON.parse(data);
        let Result = "";
        myObj.forEach(function (element) {
            if (element.username == CookeValue('username=')) {

            } else {
                Result += '<table class="rounded p-2 mb-4">\n' +
                    '                <tr>\n' +
                    '                    <td>\n' +
                    '                        <img src="pic/' + element.pro_pic + '" class="w-75 rounded shadow-sm m-2">\n' +
                    '                    </td>\n' +
                    '                    <td class="text-left">\n' +
                    '                        ' + element.first_name + ' ' + element.last_name + '<br>\n' +
                    '                        ' + element.email + '\n' +
                    '                    </td>\n' +
                    '                </tr>\n' +
                    '            </table>';
            }
        });
        $(".AllUser").html(Result);
    });
}

function Setting() {
    let Obj = '["*"]';
    $.post('home/crud.php?action=dashboard', {
            PostTable: "sign_up",
            PostData: Obj,
            PostUsername: CookeValue('username=')
        },
        function (data) {
            JSON.parse(data).forEach(function (element) {
                $("#setting").css({"display":"flex"});
                $("#b_day").val(element.b_day);
                $("#b_month").val(element.b_month);
                $("#b_year").val(element.b_year);
                $("#SignFirstName").val(element.first_name);
                $("#SignLastName").val(element.last_name);
                $("#SignEmail").val(element.email);
                let hjk = "input[value='"+element.gender+"']";
                $(hjk).prop("checked",true);
            });
        });
}

/**
 * @return {string}
 */
function CookeValue(name) {
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) !== 0) {
        } else {
            return c.substring(name.length, c.length);
        }
    }
}

function UpdateInfo() {
    let Obj = '["*"]';
    $.post('home/crud.php?action=dashboard', {
            PostTable: "about",
            PostData: Obj,
            PostUsername: CookeValue('username=')
        },
        function (data) {
        let ValueArray = "";
            JSON.parse(data).forEach(function (element) {
                ValueArray = ["", "", "", element.relationship, element.phone, "", "", element.languages, element.religious_views, element.high_school, element.collage, element.current_city, element.hometown, element.interested_in, element.political_views, ""];
            });
            for (let i = 1; i <= 15; i++) {
                if (i === 1 || i === 2 || i === 5 || i === 6) {

                } else if (i === 4) {
                    $(".input" + i).html('<input type="number" placeholder="Input" id="input' + i + '" value="' + ValueArray[i] + '" onkeyup="valueChange(' + i + ')">');
                    $(".input" + i + " input").animate({width: '100%'});
                } else if (i === 15) {
                    $(".input" + i).html('<textarea placeholder="Input" class="w-100" id="input' + i + '">' + ValueArray[i] + '</textarea>');
                    $(".input" + i + " input").animate({width: '100%'});
                } else {
                    $(".input" + i).html('<input type="text" placeholder="Input" id="input' + i + '" value="' + ValueArray[i] + '" onkeyup="valueChange(' + i + ')">');
                    $(".input" + i + " input").animate({width: '100%'});
                }
            }
            let BookableArr = JSON.stringify(ValueArray);
            document.cookie = "userInfo=" + BookableArr;
        });
    $(".updateInfoBtn").html('<button class="float-right btn btn-secondary btn-sm" onclick="UserProfile()">Cancel</button> <button class="float-right btn btn-primary btn-sm mr-2 SaveBtn" onclick="SaveEdit()" disabled>Save</button>').delay(2000);
}

function valueChange(value) {
    let userInfo = JSON.parse(CookeValue("userInfo="));
    let input = $("#input"+value).val();
    let inputID = "#input"+value;
    if (input === userInfo[value]){
        let Change = JSON.parse(CookeValue("Change="));
        for (let i=0;i<=Change.length;i++){
            if (inputID === Change[i]){
                Change.splice(i, 1);
                Change = JSON.stringify(Change);
                document.cookie = "Change="+Change;
            }
        }
    }else {
        if (CookeValue("Change=") === undefined){
            let Change = [inputID];
            Change = JSON.stringify(Change);
            document.cookie = "Change="+Change;
        }else {
            let Change = JSON.parse(CookeValue("Change="));
            for (let i=0;i<=Change.length;i++){
                if (inputID === Change[i]){
                    break;
                } else {
                    Change.unshift(inputID);
                    Change = JSON.stringify(Change);
                    document.cookie = "Change="+Change;
                }
            }
        }
    }
    let Change = JSON.parse(CookeValue("Change="));
    if (Change.length !== 0){
        $('.SaveBtn').removeAttr("disabled");
    }else {
        $('.SaveBtn').attr("disabled", true);
    }
}

function SaveEdit() {
    let name = "";
    let myObj = [];
    let Change = JSON.parse(CookeValue("Change="));
    for (let i=0;i<Change.length;i++){

        switch (Change[i]) {
            case "#input3":
                name = "relationship";
                break;
            case "#input4":
                name = "phone";
                break;
            case "#input7":
                name = "languages";
                break;
            case "#input8":
                name = "religious_views";
                break;
            case "#input9":
                name = "high_school";
                break;
            case "#input10":
                name = "collage";
                break;
            case "#input11":
                name = "current_city";
                break;
            case "#input12":
                name = "hometown";
                break;
            case "#input13":
                name = "interested_in";
                break;
            case "#input14":
                name = "political_views";
                break;
            case "#input15":
                name = "details_about_you";
        }

        let IdName = $(Change[i]).val();
        let Obj = '{"'+name+'":"'+IdName+'"}';
        let firstObject = JSON.parse(Obj);
        Object.assign(myObj, firstObject);


        if(i === Change.length - 1){
            let JsonObj = JSON.stringify(myObj);
            $.post('home/crud.php?action=update', {
                    PostTable: "about",
                    PostData: JsonObj,
                    PostUsername: CookeValue('username=')
                },
                function (data) {
                    if(data === "done"){
                        UserProfile();
                    }else {
                        alert("Something Wrong");
                    }
                });
        }
    }
}









