<?php
//Mysql connection
include_once('../database/sql_connect.php');

class READ extends Database
{
    public function GET($Username, $Table, $Data)
    {
        $info = "";
        foreach ($Data as $row) {
            $info .= "$row, ";
        }
        $info = substr($info, 0, -2);
        if ($Username) {
            $sql = "SELECT $info FROM" . " $Table WHERE username='$Username'";
        } else {
            $sql = "SELECT $info FROM" . " $Table";
        }
        $array = [];
        $query = $this->conn->query($sql);
        while ($resource = mysqli_fetch_assoc($query)) {
            $array[] = $resource;
        }
        return $array;
    }

    public function Update($Username, $Table, $Data)
    {
        $sql = "";
        foreach ($Data as $key => $value) {
            $sql .= $key . "='" . $value . "', ";
        }
        $sql = substr($sql, 0, -2);
        $sql = "UPDATE " . $Table . " SET " . $sql . " WHERE username='" . $Username . "'";
        if (mysqli_query($this->conn, $sql)) {
            return true;
        } else {
            return false;
        }
    }

}


if (isset($_GET['action']) && $_GET['action'] == 'dashboard') {
    $Object = new READ();
    $Table = $_POST["PostTable"];
    $Data = json_decode($_POST["PostData"]);
    if (isset($_POST["PostUsername"])) {
        $Username = $_POST["PostUsername"];
    } else {
        $Username = false;
    }
    $Result = $Object->GET($Username, $Table, $Data);
    echo json_encode($Result);

}

if (isset($_GET['action']) && $_GET['action'] == 'update') {
    $Object = new READ();
    $Table = $_POST["PostTable"];
    $Data = json_decode($_POST["PostData"]);
    $Username = $_POST["PostUsername"];
    $Result = $Object->Update($Username, $Table, $Data);
    if ($Result) {
        echo "done";
    }

}