<?php
//Log_in Check
include_once('log_check.php');

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <!-- Bootstrap core CSS -->
    <link href="framework/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="framework/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="framework/fontawesome/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!---Custom CSS apply this website--->
    <link rel="stylesheet" type="text/css" href="home/style.css">

</head>
<body>

<div class="container mt-5">
    <div class="row">
        <div class="MenuBar col-md-3 col-12">
            <img src="pic/<?php if (isset($Image)) {
                echo $Image;
            } ?>" class="w-25 rounded shadow-sm" alt="Nayem">
            <?php if (isset($Name)) {
                echo $Name;
            } ?>
            <ul class="mt-5 list-group list-unstyled text-left">
                <li class="my-2 mt-5 CG-1" onclick="view(5)"><span class="fa">&#xf0e4;</span> DASHBOARD</li>
                <li class="my-2 CG-2" onclick="view(4)"><span class="ion ion-person"></span> USER PROFILE</li>
                <li class="my-2 CG-3" onclick="view(3)"><span class="material-icons">&#xe7ef;</span> All USER</li>
                <li class="my-2 CG-4" onclick="view(2)"><span class="ion ion-android-settings"></span> SECURITY</li>
                <li class="my-2"><a href="index.php?action=logout"><span class="ion ion-log-out"></span> LOG OUT</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-12">
            <div id="view" class="">
                <div class="row" id="setting">
                    <div class="col-md-7 mr-2">
                        <div class="row">
                            <div class="col-12 mb-3 bg-dark p-3 rounded">
                                <p class="h4">Change Name:</p>
                                First Name: <input type="text" class="input-text" id="SignFirstName"> <br>
                                Last Name: <input type="text" class="input-text" id="SignLastName">
                                <button type="button" class="btn btn-primary float-right btn-sm mt-2 mr-2">Save
                                </button>
                            </div>
                            <div class="col-12 bg-dark p-3 rounded">
                                <p class="h4">Change Email:</p>
                                Email: <input type="text" class="input-text" id="SignEmail">
                                <button type="button" class="btn btn-primary float-right btn-sm mt-2 mr-2">Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bg-dark p-3 rounded">
                        <p class="h4">Change Password:</p>
                        Old Password: <input type="text" class="input-text w-100"> <br>
                        New Password: <input type="text" class="input-text w-100"><br>
                        Retype Password: <input type="text" class="input-text w-100">
                        <button type="button" class="btn btn-primary float-right btn-sm mt-2">Save</button>
                    </div>
                    <div class="col-md-7 bg-dark rounded p-3 mt-3">
                        Date of Birth:
                        <select onblur="date()" id="b_day">
                            <option value="" selected hidden>Day</option>
                            <?php

                            for ($i = 1; $i <= 31; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                            }

                            ?>
                        </select>
                        <select onblur="date()" id="b_month">
                            <option value="" selected hidden>Month</option>

                            <?php
                            $allMonth = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                            $i = 0;
                            foreach ($allMonth as $values) {
                                echo '<option value="' . $i . '">' . $values . '</option>';
                                $i++;
                            }
                            ?>
                        </select>
                        <select onblur="date()" id="b_year">
                            <option value="" selected hidden>Year</option>
                            <?php

                            for ($i = 1961; $i <= 2018; $i++) {
                                    echo '<option value="' . $i . '">' . $i . '</option>';
                            }

                            ?>
                        </select><br><br>
                        Gender:
                        <input type="radio" name="gender" value="Female" id="input_gender">Female
                        <input type="radio" name="gender" value="Male" id="input_gender">Male
                        <input type="radio" name="gender" value="Other" id="input_gender">Other<br>
                        <button type="button" class="btn btn-primary btn-sm mt-2">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- JavaScript Libraries -->
<script src="framework/jquery/jquery.min.js"></script>
<script src="framework/jquery/jquery-migrate.min.js"></script>
<script src="framework/jquery-easing/jquery.easing.min.js"></script>

<!-- Bootstrap core JS -->
<script src="framework/bootstrap/js/bootstrap.bundle.min.js"></script>

<!---Custom Javascript apply this website--->
<script type="text/javascript" src="home/home.js"></script>
</body>
</html>
