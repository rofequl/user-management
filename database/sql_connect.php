<?php

class Database{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "project_oop";
    public $conn = "";

    public function __construct(){
        $this->conn = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
    }
}

$obj = new Database();