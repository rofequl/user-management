// ========================================================================= //
//  Function run when the page open, Loading page hide delay 5000
// ========================================================================= //
$(document).ready(function () {
    $(".FirstLoad").delay(0).hide(0);
});

// ========================================================================= //
//  Login, Signup and Forget Password bar show.
// ========================================================================= //
$(document).ready(function () {
    $("#SignUp-Btn").click(function () {
        $("#Login").fadeOut(1500);
        $("#SignUp").delay(1500).fadeIn(1500);
    });

    $("#ForPass-Btn").click(function () {
        $("#Login").fadeOut(1500);
        $("#ForPass").delay(1500).fadeIn(1500);
    });

    $("#LogIn-Btn").click(function () {
        $("#SignUp").fadeOut(1500);
        $("#Login").delay(1500).fadeIn(1500);
    });

    $("#LogIn-BtnA").click(function () {
        $("#ForPass").fadeOut(1500);
        $("#Login").delay(1500).fadeIn(1500);
    });

});


// ========================================================================= //
//  Login Email, Password Check.
// ========================================================================= //
/**
 * @return {boolean}
 */
function LogEmail() {
    let name = $("#LogInputEmail").val();
    let na = /^[\w\-.+]+@[a-zA-Z0-9.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (name === "") {
        $("#Log-Email1").css({"visibility": "visible"}).html("Enter your Email Id");
        return false;
    } else if (name.match(na)) {
        $("#Log-Email1").css({"visibility": "hidden"}).html("");
        return true;
    } else {
        $("#Log-Email1").css({"visibility": "visible"}).html("Enter Valid Email Id..");
        return false;
    }
}

/**
 * @return {boolean}
 */
function LogPass() {
    let name = $('#LogInputPass').val();
    if (name === "") {
        $("#Log-Password").css({"visibility": "visible"}).html("Enter your password");
        return false;
    } else if (name.length < 8 || name.length > 20) {
        $("#Log-Password").css({"visibility": "visible"}).html("Your Password must be 8 to 20");
        return false;
    } else {
        $("#Log-Password").css({"visibility": "hidden"}).html("");
        return true;
    }
}


/*  Submit button click, Email Password check and LogIn. */
$("#LogIn-Form").submit(function (event) {
    LogEmail();
    LogPass();
    if (LogEmail() !== true) {
    } else if (LogPass() !== true) {
    }
    else {
        let email = $("#LogInputEmail").val();
        let password = $('#LogInputPass').val();
        let remember = $('#LogInputRem:checked').val();
        if (!remember) {
            remember = "";
        }
        $.post('sign_log.php?action=L5O4G3I2N1', {
                PostEmail: email,
                PostPass: password,
                PostRemember: remember
            },
            function (data) {
                if (data == "gg") {
                    $("#Log-Email1").css({"visibility": "visible"}).html("Email Id not match.");
                } else if (data == "ff") {
                    $("#Log-Password").css({"visibility": "visible"}).html("Your Password Incorrect..");
                } else if (data == "ll") {
                    window.location.replace("../index.php");
                } else {
                    alert("Something wrong, we fix the problem..");
                }
            });
    }

    event.preventDefault();
});


// ========================================================================= //
//  Sign up First Name, Last Name, Email, Password, Re Password
// ========================================================================= //
/**
 * @return {boolean}
 */
function SignInputFirstName() {
    let name = $("#SignFirstName").val();
    let word = name.charAt(0);
    let numeric = /^[0-9]+$/;
    if (name === "") {
        $("#Sign-First-Name").css({"visibility": "visible"}).html("Enter your first name.");
        $("#SignFirstName").css({"border": "2px solid red"});
        return false;
    } else if (word.match(numeric)) {
        $("#Sign-First-Name").css({"visibility": "visible"}).html("First word must be a latter.");
        $("#SignFirstName").css({"border": "2px solid red"});
        return false;
    } else {
        $("#Sign-First-Name").css({"visibility": "hidden"}).html("");
        $("#SignFirstName").css({"border": "1px solid #8c5858"});
        return true;
    }
}

/**
 * @return {boolean}
 */
function SignInputLastName() {
    let name = $("#SignLastName").val();
    let word = name.charAt(0);
    let numeric = /^[0-9]+$/;
    if (name === "") {
        $("#Sign-Last-Name").css({"visibility": "visible"}).html("Enter your last name.");
        $("#SignLastName").css({"border": "2px solid red"});
        return false;
    } else if (word.match(numeric)) {
        $("#Sign-Last-Name").css({"visibility": "visible"}).html("First word must be a latter.");
        $("#SignLastName").css({"border": "2px solid red"});
        return false;
    } else {
        $("#Sign-Last-Name").css({"visibility": "hidden"}).html("");
        $("#SignLastName").css({"border": "1px solid #8c5858"});
        return true;
    }
}

/**
 * @return {boolean}
 */
function SignInputEmail() {
    let name = $("#SignEmail").val();
    let na = /^[\w\-.+]+@[a-zA-Z0-9.\-]+\.[a-zA-z0-9]{2,4}$/;
    let back = true;
    
    if (name === "") {
        $("#Sign-Email").css({"visibility": "visible"}).html("Enter your Email Id.");
        $("#SignEmail").css({"border": "2px solid red"});
        return false;
    } else if (name.match(na)) {
        $.post('sign_log.php?action=E5M4A3I2L1', {PostEmail: name},
            function (data) {
                if (data == "1") {
                    $("#Sign-Email").css({"visibility": "visible"}).html("Already This Email Use To Create An Account.");
                    $("#SignEmail").css({"border": "2px solid red"});
                    back = false;
                } else if (data == "0") {
                    $("#Sign-Email").css({"visibility": "hidden"}).html("");
                    $("#SignEmail").css({"border": "1px solid #8c5858"});
                    back = true;
                } else {
                    $("#Sign-Email").css({"visibility": "visible"}).html("Something wrong, We fix the problem.");
                    $("#SignEmail").css({"border": "2px solid red"});
                    back = false;
                }
            });return back;
    } else {
        $("#Sign-Email").css({"visibility": "visible"}).html("Enter Valid Email Id..");
        $("#SignEmail").css({"border": "2px solid red"});
        return false;
    }
}

/**
 * @return {boolean}
 */
function SignInputPass() {
    let name = $('#SignPass').val();
    if (name === "") {
        $("#Sign-Pass").css({"visibility": "visible"}).html("Enter your password");
        $("#SignPass").css({"border": "2px solid red"});
        return false;
    } else if (name.length < 8 || name.length > 20) {
        $("#Sign-Pass").css({"visibility": "visible"}).html("Your Password must be 8 to 20");
        $("#SignPass").css({"border": "2px solid red"});
        return false;
    } else {
        $("#Sign-Pass").css({"visibility": "hidden"}).html("");
        $("#SignPass").css({"border": "1px solid #8c5858"});
        return true;
    }
}

/**
 * @return {boolean}
 */
function SignInputRePass() {
    let name = $('#SignPass').val();
    let rename = $('#SignRePass').val();
    if (rename === "") {
        $("#Sign-Re-Pass").css({"visibility": "visible"}).html("Enter your Re password");
        $("#SignRePass").css({"border": "2px solid red"});
        return false;
    } else if (SignInputPass() !== true) {
        $("#Sign-Re-Pass").css({"visibility": "visible"}).html("Password not match.");
        $("#SignRePass").css({"border": "2px solid red"});
        return false;
    } else if (name.length !== rename.length) {
        $("#Sign-Re-Pass").css({"visibility": "visible"}).html("Password not match.");
        $("#SignRePass").css({"border": "2px solid red"});
        return false;
    } else if (rename.match(name)) {
        $("#Sign-Re-Pass").css({"visibility": "hidden"}).html("");
        $("#SignRePass").css({"border": "1px solid #8c5858"});
        return true;
    } else {
        $("#Sign-Re-Pass").css({"visibility": "visible"}).html("Password not match.");
        $("#SignRePass").css({"border": "2px solid red"});
        return false;
    }
}

/**
 * @return {boolean}
 */
function SignInputDob() {
    let day = $('#b_day').val();
    let month = $('#b_month').val();
    let year = $('#b_year').val();
    let ms = (new Date(year, month, day)).getTime();
    let aoDate = new Date();
    aoDate.setTime(ms);
    if (day === "" || month === "" || year === "") {
        $("#Sign-Date").css({"visibility": "visible"}).html("Enter your Date of Birth.");
        $("#b_day").css({"border": "2px solid red"});
        $("#b_month").css({"border": "2px solid red"});
        $("#b_year").css({"border": "2px solid red"});
        return false;
    } else if (aoDate.getFullYear() != year || aoDate.getMonth() != month || aoDate.getDate() != day) {
        $("#Sign-Date").css({"visibility": "visible"}).html("Invalid Date of Birth.");
        return false;
    } else {
        $("#Sign-Date").css({"visibility": "hidden"}).html("");
        $("#b_day").css({"border": "1px solid #8c5858"});
        $("#b_month").css({"border": "1px solid #8c5858"});
        $("#b_year").css({"border": "1px solid #8c5858"});
        return true;
    }


}

/**
 * @return {boolean}
 */
function SignInputGender() {
    let name = $('#input_gender:checked').val();
    if (name === undefined || name === null) {
        $("#Sign-Gender").css({"visibility": "visible"}).html("Select Gender.");
        return false;
    } else {
        $("#Sign-Gender").css({"visibility": "hidden"}).html("");
        return true;
    }

}


/*  Submit button click, Sign up form check and LogIn. */

$("#SignUp-Form").submit(function (event) {
    SignInputFirstName();
    SignInputLastName();
    SignInputEmail();
    SignInputPass();
    SignInputRePass();
    SignInputDob();
    SignInputGender();

    if (SignInputFirstName() !== true) {
    } else if (SignInputLastName() !== true) {
    } else if (SignInputEmail() !== true) {
    } else if (SignInputPass() !== true) {
    } else if (SignInputRePass() !== true || SignInputDob() !== true) {
    } else {

        let first_name = $("#SignFirstName").val();
        let last_name = $("#SignLastName").val();
        let email = $("#SignEmail").val();
        let password = $('#SignPass').val();
        let day = $('#b_day').val();
        let month = $('#b_month').val();
        let year = $('#b_year').val();
        let gender = $('#input_gender:checked').val();
        let username = first_name + last_name;
        let Obj = '{"first_name":"' + first_name + '","last_name":"' + last_name + '","email":"' + email + '"' +
            ',"password":"' + password + '","b_day":"' + day + '","b_month":"' + month + '","b_year":"' + year + '"' +
            ',"gender":"' + gender + '","username":"' + username + '","position":"ACTIVATE","pro_pic":"img_avatar.png"}';

        $.post('sign_log.php?action=S5I4G3N2U1P', {SignUp: Obj},
            function (data) {
                if(data == 1){
                    window.location.replace("../index.php");
                }else {
                    alert("Something Wrong, Please try again leter.");
                }
            });
    }
    event.preventDefault();
});


// ========================================================================= //
//  Forget Password, Email Birthday Check.
// ========================================================================= //

/**
 * @return {boolean}
 */
function ForPassEmail() {
    let name = $("#ForPassInputEmail").val();
    let na = /^[\w\-.+]+@[a-zA-Z0-9.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (name === "") {
        $("#ForPass-Email1").css({"visibility": "visible"}).html("Enter your Email Id");
        return false;
    } else if (name.match(na)) {
        $("#ForPass-Email1").css({"visibility": "hidden"}).html("");
        return true;
    } else {
        $("#ForPass-Email1").css({"visibility": "visible"}).html("Enter Valid Email Id..");
        return false;
    }
}

/**
 * @return {boolean}
 */
function ForPassInputDob() {
    let day = $('#Forb_day').val();
    let month = $('#Forb_month').val();
    let year = $('#Forb_year').val();
    let ms = (new Date(year, month, day)).getTime();
    let aoDate = new Date();
    aoDate.setTime(ms);
    if (day === "" || month === "" || year === "") {
        $("#ForPass-Date").css({"visibility": "visible"}).html("Enter your Date of Birth.");
        return false;
    } else if (aoDate.getFullYear() != year || aoDate.getMonth() != month || aoDate.getDate() != day) {
        $("#ForPass-Date").css({"visibility": "visible"}).html("Invalid Date of Birth.");
        return false;
    } else {
        $("#ForPass-Date").css({"visibility": "hidden"}).html("");
        return true;
    }
}

$("#ForPass-Form").submit(function (event) {
    ForPassEmail();
    ForPassInputDob();
    if (ForPassEmail() !== true) {
    } else if (ForPassInputDob() !== true) {
    } else {
        let name = $("#ForPassInputEmail").val();
        let day = $('#Forb_day').val();
        let month = $('#Forb_month').val();
        let year = $('#Forb_year').val();
        $.post('sign_log.php?action=F5O4R3P2A1S0S', {
                PostEmail: name,
                PostDay: day,
                PostMonth: month,
                PostYear: year
            },
            function (data) {
                if(data == 1){
                    $("#ForPass-Form").css({"display": "none"});
                    $("#ForPass-FormAgain").css({"display": "block"});
                }else if(data == 0){
                    alert("Wrong Information..")
                }else{
                    alert(data);
                }
            });
    }

    event.preventDefault();
});

/**
 * @return {boolean}
 */
function ForPassPassword() {
    let name = $('#ForPassInputPassword').val();
    if (name === "") {
        $("#ForPass-Password").css({"visibility": "visible"}).html("Enter your password");
        $("#ForPassInputPassword").css({"border": "2px solid red"});
        return false;
    } else if (name.length < 8 || name.length > 20) {
        $("#ForPass-Password").css({"visibility": "visible"}).html("Your Password must be 8 to 20");
        $("#ForPassInputPassword").css({"border": "2px solid red"});
        return false;
    } else {
        $("#ForPass-Password").css({"visibility": "hidden"}).html("");
        $("#ForPassInputPassword").css({"border": "1px solid #8c5858"});
        return true;
    }
}

/**
 * @return {boolean}
 */
function ForPassRePassword() {
    let name = $('#ForPassInputPassword').val();
    let rename = $('#ForPassInputRePassword').val();
    if (rename === "") {
        $("#ForPass-RePassword").css({"visibility": "visible"}).html("Enter your Re password");
        $("#ForPassInputRePassword").css({"border": "2px solid red"});
        return false;
    } else if (ForPassPassword() !== true) {
        $("#ForPass-RePassword").css({"visibility": "visible"}).html("Password not match.");
        $("#ForPassInputRePassword").css({"border": "2px solid red"});
        return false;
    } else if (name.length !== rename.length) {
        $("#ForPass-RePassword").css({"visibility": "visible"}).html("Password not match.");
        $("#ForPassInputRePassword").css({"border": "2px solid red"});
        return false;
    } else if (rename.match(name)) {
        $("#ForPass-RePassword").css({"visibility": "hidden"}).html("");
        $("#ForPassInputRePassword").css({"border": "1px solid #8c5858"});
        return true;
    } else {
        $("#ForPass-RePassword").css({"visibility": "visible"}).html("Password not match.");
        $("#ForPassInputRePassword").css({"border": "2px solid red"});
        return false;
    }
}


$("#ForPass-FormAgain").submit(function (event) {
    ForPassPassword();
    ForPassRePassword();
    if (ForPassPassword() !== true) {
    } else if (ForPassRePassword() !== true) {
    } else {
        let name = $('#ForPassInputPassword').val();
        $.post('sign_log.php?action=A5G4A3I2N1', {PostPassword: name},
            function (data) {
                if(data == 1){
                    window.location.replace("index.php");
                }else {
                    alert("Something Wrong..")
                }
            });
    }
    event.preventDefault();
});





















