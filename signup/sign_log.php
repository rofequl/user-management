<?php
//Mysql connection
include_once('../database/sql_connect.php');

class SignClass extends Database
{
    public function UserName($data)
    {
        $data = strtolower($data);
        $data = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $data);
        $data = preg_replace('/\s+/', '', $data);
        $user = "SELECT * FROM" . " sign_up WHERE username='$data'";
        $query = mysqli_query($this->conn, $user);
        $rowcount = mysqli_num_rows($query);
        return $data . $rowcount;
    }

    public function InsertRecord($update)
    {

        $password = password_hash($update["password"], PASSWORD_DEFAULT, array('cost' => 10));
        $Username = $this->UserName($update["username"]);
        $a2 = array("password" => $password, "username" => $Username);
        $new = array_replace($update, $a2);
        $sql = "";
        $sql .= "INSERT INTO" . " sign_up";
        $sql .= " (" . implode(", ", array_keys($new)) . ") VALUES ";
        $sql .= "('" . implode("','", array_values($new)) . "')";
        $sql2 = "INSERT INTO" . " about (username,online) VALUES ('" . $new["username"] . "','01')";
        $query2 = mysqli_query($this->conn, $sql2);
        $query = mysqli_query($this->conn, $sql);
        if ($query && $query2) {
            setcookie('username', $Username, time() + (86400 * 30), "/");
            setcookie('password', $password, time() + (86400 * 30), "/");
            return "1";
        } else {
            return "0";
        }
    }
}

class Email extends Database
{
    public function CheckEmail($Email)
    {
        $email = "SELECT * FROM" . " sign_up WHERE email='$Email'";
        $query = mysqli_query($this->conn, $email);
        $rowcount = mysqli_num_rows($query);
        return $rowcount;
    }
}

class LoginClass extends Database
{
    public function LogIn($Email, $Password, $Remember)
    {
        $sql = "SELECT * FROM" . " sign_up WHERE email='$Email'";
        $connect = mysqli_query($this->conn, $sql);
        $rowcount = mysqli_num_rows($connect);
        if ($rowcount == 1) {
            while ($result = mysqli_fetch_array($connect)) {
                $username = $result['username'];
                $pass = $result['password'];
                if (password_verify($Password, $pass)) {
                    if ($Remember == ""){
                        setcookie('username', $username, 0, "/");
                        setcookie('password', $pass, 0, "/");
                        return "ll";
                    }else{
                        setcookie('username', $username, time() + (86400 * 30), "/");
                        setcookie('password', $pass, time() + (86400 * 30), "/");
                        return "ll";
                    }
                } else {
                    return "ff";
                }
            }
        } else {
            return "gg";
        }
        return false;
    }
}

class ForPassClass extends Database
{
    public function encryptIt($q)
    {
        $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return ($qEncoded);
    }

    public function decryptIt($q)
    {
        $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return ($qDecoded);
    }

    public function ForPass($Email, $Day, $Month, $Year)
    {
        $email = "SELECT * FROM" . " sign_up WHERE email='$Email' AND b_day='$Day' AND b_month='$Month' AND b_year='$Year'";
        $query = mysqli_query($this->conn, $email);
        $rowcount = mysqli_num_rows($query);
        if ($rowcount == 1) {
            while ($result = mysqli_fetch_array($query)) {
                $username = $this->encryptIt($result['username']);
                setcookie('emanresu', $username);
                return $rowcount;
            }
        } else {
            return $rowcount;
        }
        return false;
    }

    public function ForPassChange($Password)
    {
        if (isset($_COOKIE['emanresu'])) {
            $username = $this->decryptIt($_COOKIE['emanresu']);
            $Password = password_hash($Password, PASSWORD_DEFAULT, array('cost' => 10));
            $sql = "UPDATE" . " sign_up SET password='$Password' WHERE username='$username'";
            if (mysqli_query($this->conn, $sql)) {
                return 1;
            } else {
                return 0;
            }
        }
        return false;
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'S5I4G3N2U1P') {
    $SignUp = $_POST["SignUp"];
    $update = json_decode($SignUp, true);
    $object = new SignClass();
    $submit = $object->InsertRecord($update);
    echo $submit;
}


if (isset($_GET['action']) && $_GET['action'] == 'E5M4A3I2L1') {
    $Email = $_POST["PostEmail"];
    $object = new Email();
    $submit = $object->CheckEmail($Email);
    echo $submit;
}


if (isset($_GET['action']) && $_GET['action'] == 'L5O4G3I2N1') {
    $Email = $_POST["PostEmail"];
    $Password = $_POST["PostPass"];
    $Remember = $_POST["PostRemember"];
    $object = new LoginClass();
    $submit = $object->LogIn($Email, $Password, $Remember);
    echo $submit;
}

if (isset($_GET['action']) && $_GET['action'] == 'F5O4R3P2A1S0S') {
    $Email = $_POST["PostEmail"];
    $Day = $_POST["PostDay"];
    $Month = $_POST["PostMonth"];
    $Year = $_POST["PostYear"];
    $object = new ForPassClass();
    $submit = $object->ForPass($Email, $Day, $Month, $Year);
    echo $submit;
}

if (isset($_GET['action']) && $_GET['action'] == 'A5G4A3I2N1') {
    $Password = $_POST["PostPassword"];
    $object = new ForPassClass();
    $submit = $object->ForPassChange($Password);
    echo $submit;
}


