<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign-up Marhabaan Alealam</title>
    <!-- Bootstrap core CSS -->
    <link href="../framework/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../framework/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom Style for this site-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>

<!-- start section Loading Design -->
<section class="FirstLoad">
    <ul id="progress">
        <li>
            <div id="layer1" class="ball"></div> <!-- layer1 control delay animation / ball is effect -->
            <div id="layer7" class="pulse"></div> <!-- layer7 control delay animation / pulse is effect  -->
        </li>
        <li>
            <div id="layer2" class="ball"></div>
            <div id="layer8" class="pulse"></div>
        </li>
        <li>
            <div id="layer3" class="ball"></div>
            <div id="layer9" class="pulse"></div>
        </li>
        <li>
            <div id="layer4" class="ball"></div>
            <div id="layer10" class="pulse"></div>
        </li>
        <li>
            <div id="layer5" class="ball"></div>
            <div id="layer11" class="pulse"></div>
        </li>
    </ul>
</section>
<!-- End section Loading Design -->


<!-- start section Log in Bar -->
<section id="Login">
    <div class="p-3 rounded">
        <header>
            Login
            <button class="btn float-right mt-2" id="SignUp-Btn">Sign up</button>
        </header>
        <form method="post" action="" class="mt-3" id="LogIn-Form">
            <div class="Alert" id="Log-Email1"></div>
            <i class="fa fa-envelope"></i><input type="email" placeholder="Enter Email Id" maxlength="40"
                                                 onkeyup="LogEmail()" id="LogInputEmail">
            <div class="Alert" id="Log-Password"></div>
            <i class="fa fa-lock"></i><input type="password" placeholder="Enter Password" maxlength="21"
                                             id="LogInputPass" onkeyup="LogPass()">
            <input class="btn w-100" type="Submit">
        </form>
        <footer class="my-2">
            <span>
                <label class="switch">
                    <input type="checkbox" id="LogInputRem">
                    <span class="slider round"><i>&#x2714;</i></span>
                </label>
                    Remember me
            </span>
            <span class="float-right" id="ForPass-Btn">Lost your password?</span>
        </footer>
    </div>
</section>
<!-- End section Log in Bar -->


<!-- start section Sign up Bar -->
<section id="SignUp">
    <div class="p-3 rounded">
        <header>
            Sign up
            <button class="btn float-right mt-2" id="LogIn-Btn">Login</button>
        </header>
        <form method="post" action="" class="mt-3" id="SignUp-Form">
            <div class="Alert" id="Sign-First-Name"></div>
            <input type="text" placeholder="First Name" maxlength="40" class="w-100" id="SignFirstName"
                   onkeyup="SignInputFirstName()">
            <div class="Alert" id="Sign-Last-Name"></div>
            <input type="text" placeholder="Last Name" maxlength="40" class="w-100" id="SignLastName"
                   onkeyup="SignInputLastName()">
            <div class="Alert" id="Sign-Email"></div>
            <input type="email" placeholder="Email" maxlength="40" class="w-100" id="SignEmail"
                   onkeyup="SignInputEmail()">
            <div class="Alert" id="Sign-Pass"></div>
            <input type="password" placeholder="Enter Password" class="w-100" maxlength="40" id="SignPass"
                   onkeyup="SignInputPass()">
            <div class="Alert" id="Sign-Re-Pass"></div>
            <input type="password" placeholder="Re Enter Password" class="w-100" maxlength="40" id="SignRePass"
                   onkeyup="SignInputRePass()">
            <div class="Alert" id="Sign-Date"></div>
            <div class="birthGender">Birthday:
                <label for="b_day"></label><select onblur="date()" id="b_day">
                    <option value="" selected hidden>Day</option>
                    <?php

                    for ($i = 1; $i <= 31; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }

                    ?>
                </select>
                <label for="b_month"></label><select onblur="date()" id="b_month">
                    <option value="" selected hidden>Month</option>

                    <?php

                    $allMonth = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                    $i = 0;
                    foreach ($allMonth as $values) {
                        echo '<option value="' . $i . '">' . $values . '</option>';
                        $i++;
                    }


                    ?>
                </select>
                <label for="b_year"></label><select onblur="date()" id="b_year">
                    <option value="" selected hidden>Year</option>
                    <?php

                    for ($i = 1961; $i <= 2018; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }

                    ?>
                </select>
                <div class="Alert" id="Sign-Gender"></div>
                <div class="form-check">
                    Gender:
                    <label class="form-check-label ml-4">
                        <input type="radio" class="form-check-input" value="Male" id="input_gender" name="optradio">Male
                    </label>
                    <label class="form-check-label ml-4">
                        <input type="radio" class="form-check-input" value="Female" id="input_gender" name="optradio">Female
                    </label>
                    <label class="form-check-label ml-4">
                        <input type="radio" class="form-check-input" value="Other" id="input_gender" name="optradio">Other
                    </label>
                </div>
            </div>
            <input class="btn w-100" type="Submit">
        </form>
    </div>
</section>
<!-- End section Sign up Bar -->


<!-- start section Forget Password Bar -->
<section id="ForPass">
    <div class="p-3 rounded mt-5">
        <header>
            Forgotten Account
        </header>
        <form method="post" action="" class="mt-3" id="ForPass-Form">
            <div class="Alert" id="ForPass-Email1"></div>
            <i class="fa fa-envelope"></i><input type="email" placeholder="Enter Email Id" maxlength="40"
                                                 onkeyup="ForPassEmail()" id="ForPassInputEmail">
            <div class="Alert" id="ForPass-Date"></div>
            <div class="birthGender">Birthday:
                <label for="Forb_day"></label><select onblur="date()" id="Forb_day">
                    <option value="" selected hidden>Day</option>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select>
                <label for="Forb_month"></label><select onblur="date()" id="Forb_month">
                    <option value="" selected hidden>Month</option>
                    <?php
                    $allMonth = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                    $i = 0;
                    foreach ($allMonth as $values) {
                        echo '<option value="' . $i . '">' . $values . '</option>';
                        $i++;
                    }
                    ?>
                </select>
                <label for="Forb_year"></label><select onblur="date()" id="Forb_year">
                    <option value="" selected hidden>Year</option>
                    <?php

                    for ($i = 1961; $i <= 2018; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select>
            </div>
            <input class="btn w-50" type="Submit">
            <input class="btn w-25" id="LogIn-BtnA" type="button" value="Cancel">
        </form>
        <form method="post" action="" class="mt-3" id="ForPass-FormAgain">
            <div class="Alert" id="ForPass-Password"></div>
            <i class="fa fa-lock"></i><input type="password" placeholder="Enter Password." maxlength="40"
                                                 onkeyup="ForPassPassword()" id="ForPassInputPassword">
            <div class="Alert" id="ForPass-RePassword"></div>
            <i class="fa fa-lock"></i><input type="password" placeholder="Re Enter Password." maxlength="40"
                                                 onkeyup="ForPassRePassword()" id="ForPassInputRePassword">
            <input class="btn w-100" type="Submit">
        </form>
    </div>
</section>
<!-- End section Forget Password Bar -->


<script src="../framework/jquery/jquery.min.js"></script>
<script src="../framework/jquery/jquery-migrate.min.js"></script>
<script src="../framework/jquery-easing/jquery.easing.min.js"></script>
<script src="../framework/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>